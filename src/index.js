import React from 'react';
import ReactDom from 'react-dom';
import TrackList from './components/tracklist';
import { configureStore } from './store';
import * as actions from './actions';

const tracks = [
    {
        id: 1,
        title: 'We don\'t talk anymore'
    },
    {
        id: 2,
        title: 'One more night'
    }
];

const store = configureStore();
store.dispatch(actions.setTracks(tracks));

ReactDom.render(
    <TrackList  tracks = {tracks} />,
    document.getElementById('app')
);