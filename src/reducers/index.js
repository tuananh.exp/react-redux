import { combineReducers } from 'redux';
import track from './tracks';

export default combineReducers({
    track
})