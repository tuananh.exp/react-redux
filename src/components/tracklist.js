import React, { Component, PropTypes } from 'react';

export default class TrackList extends Component {
    static propTypes = {
        tracks : PropTypes.array
    }

    static defaultProp = {
        tracks : []
    }

    render() {
        return (
            <ul>
                {
                    this.props.tracks.map((track, index) => {
                        return <li key= {index}>Track: {track.title}</li>;
                    })
                }
            </ul>
        )
    }
}