import ActionTypes from '../core/constant';

export function setTracks(tracks) {
    return {
        type: ActionTypes.TRACKS_SET,
        tracks
    };
};